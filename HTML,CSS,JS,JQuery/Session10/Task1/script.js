function respond(){
    let string = document.getElementById("string").value;
    let freq = {};
    for(var i = 0 ; i<string.length ; i++)
    {
        let character = string.charAt(i);
        if(freq[character])
        {
            freq[character]++;
        }
        else
        {
            freq[character] = 1;
        }
    }
    console.log(freq);
}