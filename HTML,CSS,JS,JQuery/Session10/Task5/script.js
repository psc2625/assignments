const data = {
	TCS: {
		revenue: 100000,
		expenses: {
			salaries: 30, //% of total revenue
			rent: 20,
			utilities: 15,
		},
		employees: [
			{
				name: 'joe',
				age: 30,
				role: 'Admin',
			},
			{
				name: 'Welt',
				age: 40,
				role: 'Tester',
			},
			{
				name: 'Sherlock',
				age: 45,
				role: 'Programmer',
			},
		],
	},
	GGk: {
		revenue: 500000,
		expenses: {
			salaries: 35, //% of total revenue
			rent: 10,
			utilities: 25,
		},
		employees: [
			{
				name: 'Martin',
				age: 28,
				role: 'Admin',
			},
			{
				name: 'Soe',
				age: 35,
				role: 'Tester',
			},
			{
				name: 'Ambati',
				age: 50,
				role: 'Programmer',
			},
		],
	},
	Osmosis: {
		revenue: 800000,
		expenses: {
			salaries: 40, //% of total revenue
			rent: 15,
			utilities: 30,
		},
		employees: [
			{
				name: 'Wardi',
				age: 36,
				role: 'Admin',
			},
			{
				name: 'Peter',
				age: 42,
				role: 'Tester',
			},
			{
				name: 'Albert',
				age: 28,
				role: 'Programmer',
			},
		],
	},
};

const DOM = {
	ageField: document.querySelector('#age'),
	ageCriteria: document.getElementsByName('age_criteria'),
	results: document.querySelector('.results'),
	ageLevel: document.getElementsByName('age_level'),
	sortByRolesButton: document.querySelector('#sort_by_roles'),
	calcProfit: document.querySelector('#calc_profit'),
};

let age = 0;
let age_Criteria = '';

/**
 *   age limit handlers
 */
DOM.ageCriteria.forEach((item) =>
	item.addEventListener('change', function () {
		age_Criteria = this.value;
		checkAgeLimitResult();
	}),
);
DOM.ageField.addEventListener('keyup', function () {
	age = this.value;
	checkAgeLimitResult();
});

/**
 * age level handler
 */

DOM.ageLevel.forEach((item) =>
	item.addEventListener('change', function () {
		DOM.results.innerHTML = '';

		let tempArr = [];
		for (let val in data) {
			tempArr.push(...data[val].employees);
		}
		tempArr = tempArr.reduce((acc, cur) => {
			if (!acc) {
				acc = cur;
				return acc;
			}
			if (this.value === 'youngest') {
				if (acc.age >= cur.age) {
					acc = cur;
					return acc;
				} else return acc;
			}
			if (this.value === 'oldest') {
				if (acc.age <= cur.age) {
					acc = cur;
					return acc;
				} else return acc;
			}
		}, '');

		const temp = `<div class="result_list">${tempArr.name} having age ${tempArr.age}</div>`;
		DOM.results.insertAdjacentHTML('beforeend', temp);
	}),
);

/**
 *  sort by role handler
 */

DOM.sortByRolesButton.addEventListener('click', function () {
	DOM.results.innerHTML = '';
	let roles = {};

	for (let val in data) {
		data[val].employees.forEach((item) => {
			if (!roles[item.role]) {
				roles[item.role] = [];
				roles[item.role].push(item);
			} else {
				roles[item.role] = [...roles[item.role], item];
			}
		});
	}
	for (let val in roles) {
		const headerDiv = `<div class="result_list result_list_header">${val}</div>`;
		DOM.results.insertAdjacentHTML('beforeend', headerDiv);
		roles[val].forEach((item, index) => {
			const listDiv = `<div class="result_list ${
				index % 2 === 0 ? 'alternate_result_list' : ''
			}">${item.name}</div>`;
			DOM.results.insertAdjacentHTML('beforeend', listDiv);
		});
	}
});

/**
 *  calculation of profit
 */

DOM.calcProfit.addEventListener('click', function () {
	DOM.results.innerHTML = '';
	const Res = [];

	for (let val in data) {
		const profit = calculateProfit(data[val].revenue, data[val].expenses);
		Res.push({ company: val, profit });
	}

	Res.forEach((item, index) => {
		const div = `<div class="result_list ${
			index % 2 === 0 ? 'alternate_result_list' : ''
		}">${item.company} with ${item.profit} profit</div>`;
		DOM.results.insertAdjacentHTML('beforeend', div);
	});
});

const checkAgeLimitResult = () => {
	if (age && age_Criteria) {
		const temp = [];
		for (let val in data) {
			temp.push(
				...data[val].employees.filter((item) => {
					if (age_Criteria === 'lte') {
						return item.age <= age;
					}
					if (age_Criteria === 'gt') {
						return item.age > age;
					}
				}),
			);
		}
		DOM.results.innerHTML = '';
		temp.forEach((item, index) => {
			const temp = `<div class="result_list ${
				index % 2 === 0 ? 'alternate_result_list' : ''
			}">${item.name}</div>`;
			DOM.results.insertAdjacentHTML('beforeend', temp);
		});
	}
};

const calculateProfit = (rev, exp) => {
	let profit = rev;

	for (let val in exp) {
		profit = profit - (exp[val] * rev) / 100;
	}

	return profit;
};
