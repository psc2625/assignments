function start() {
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let date = prompt("Enter the Date in a format - (m-d-Y i.e. 12-31-1990).")
    let dateArr = date.split("-");
    date = months[dateArr[0]-1] +" "+ dateArr[1]+" "+dateArr[2];
    const differenceInTime = new Date().getTime() - new Date(date).getTime();
    console.log("Seconds Lived : " + (differenceInTime/1000));
    console.log("Hours Lived : " + (differenceInTime/(1000*3600)));
    console.log("Days Lived : " + (differenceInTime/(1000*3600*24)));
    console.log("Weeks Lived : " + (differenceInTime/(1000*3600*24*7)));
    console.log("Months Lived : "+ (differenceInTime/(1000*3600*24))*0.0328);
    alert("Watch your output in console");
}