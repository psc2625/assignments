function filesSort() {
    var files =
        [
            {
                fileName: "flower",
                type: 'jpg'
            },
            {
                fileName: "family-video-clip",
                type: 'mp4'
            },
            {
                fileName: "phone-ringtone",
                type: 'mp3'
            },
            "javascript-excercise.txt",
            "learning-html-basics.rtf",
            {
                fileName: "friend-video-clip",
                type: 'mp4'
            },
            {
                fileName: "resume",
                type: 'docx'
            },
            {
                fileName: "student-report",
                type: 'csv'
            },
            {
                fileName: "sms-ringtone",
                type: 'mp3'
            },
            "html-basics.pdf",
            "dubsamsh.mp4",
            "screenshot.png",
            {
                fileName: "faculty-report",
                type: 'xlsx'
            },
            {
                fileName: "puppy",
                type: 'svg'
            }
        ]

    function checkExt(ext) {
        let temp = 'document';
        if (/(jpg|png|svg)/.test(ext)) {
            temp = 'image';
        }
        else if (/mp4/.test(ext)) {
            temp = 'video';
        }
        else if (/mp3/.test(ext)) {
            temp = 'audio';
        }
        return temp;
    }
    function reduceArray(){
        let sortArray =  files.reduce((acc, cur) => {
        const regEx = new RegExp(/(mp4|mp3|jpg|png|svg|csv)$/g);
        let ext = 'document';
        let fileName = '';
        if (typeof cur === 'string') {
            if (regEx.test(cur)) {
                ext = cur.match(regEx)[0];
                ext = checkExt(ext)
            }
            fileName = cur.replace(`.${ext}`, '');
        }
        if (typeof cur === 'object') {
            ext = checkExt(cur.type);
            fileName = cur.fileName;
        }
        if (!acc) {
            acc[ext] = [];
            acc[ext].push(fileName);
            return acc;
        }
        else {
            if (!acc[ext]) {
                acc[ext] = [];
            }
            acc[ext].push(fileName);
            return acc;
        }
    }, {});

    return sortArray;
}

    function userAccess()
    {
        const sorted = reduceArray();
        console.log(sorted);
        alert("Watch your output in console");
    }

    return userAccess;
}


var userAccess = filesSort();
document.getElementById("start").onclick = () => {
    userAccess();
}
