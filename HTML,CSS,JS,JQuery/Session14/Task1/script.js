function dataEnter() 
{
        var friends =[
                  { 
                   name:"Abhishek", 
                   dob: new Date("February 21 1999")
                  },{ 
                   name:"Ranjeet", 
                   dob: new Date("April 13 1998")
                  },{
                   name:"Mukesh",
                   dob: new Date("June 3 1999")
                  },{
                   name: "Prateek", 
                   dob: new Date("June 30 1998")
                  },{ 
                   name:"Shivam", 
                   dob: new Date("July 24 1998")
                  },{ 
                   name:"Ritika", 
                   dob: new Date("December 31 1997")
                  }
                 ]
    function localstorage() 
    {
    localStorage.setItem("friends",JSON.stringify(friends));    
    alert("Data added.");
    }

    var items = JSON.parse(localStorage.getItem("friends"));
    var d = new Date();



    function birthdayNextMonth()
    {
        console.log("Birthday in Next Month : ");
        d.toISOString();
        for( i = 0; i< items.length; i++)
        {
            items[i].dob = new Date(items[i].dob).toISOString();
        }
        items.filter((i) =>{
            const thisMonth = d.getMonth();
            const birthMonth = new Date(i.dob).getMonth();
            return birthMonth-thisMonth === 1 ? console.log(i.name):false;
        });
    }



    function birthdayOnSunday()
    {
        console.log("Birthday on Sunday : ");
        items.filter((i)=>{
            const thisYear = d.getFullYear();     
            const thisMonth = new Date(i.dob).getMonth();
            const thisDate = new Date(i.dob).getDate();
            var date = (thisMonth+1) + " "+ thisDate +" " + thisYear; 
            date = new Date(date);
            const thisDay = date.getDay();
            return thisDay === 0 ? console.log(i.name):false;
        });
    }



    function sameDayBirthday()
    {
        console.log("Birthday on same day : ");
        var flag = false;
        for ( let j = 0; j < items.length ; j++)
        {
            const birthDayj = new Date(items[j].dob).getDay();
            for ( let k = 0; k < items.length ; k++)
        {
            const birthDayk = new Date(items[k].dob).getDay();
            if(birthDayk === birthDayj && j != k) 
            flag = true;
        }
            if(flag)
            {
                console.log(items[j].name);
                flag = false;
            }
        }
    }



    function upcomingBirthday()
    {
        console.log("Upcomming Birthdays : ");
        items.filter((i)=>{
            const birthMonth = new Date(i.dob).getMonth();
            const birthDate = new Date(i.dob).getDate();
            const thisDate = d.getDate();
            const thisMonth = d.getMonth();
            if(birthMonth > thisMonth )
                console.log(i.name);
            else if(birthMonth == thisMonth)
                if(birthDate > thisDate)
                    console.log(i.name);
        });

    }



    function oldestFriend()
    {
        console.log("Oldest Friend : ");
        for ( let j = 0; j < items.length ; j++)
        {
            var flag = true;
            for ( let k = 0; k < items.length ; k++)
            {
                if(items[j].dob > items[k].dob) 
                flag = false;
            }
            if(flag)
            {
                console.log(items[j].name);
            }
        }
    }



    function addFriend()
    {
        var friendName = prompt("Enter the name of your friend");
        var friendDOB = prompt("Enter the D.O.B in this format(January 1 1997)");
        friendDOB = new Date(friendDOB);
        var obj = 
        {
            name: friendName,
            dob: friendDOB
        }
        friends.push(obj);
        localStorage.setItem("friends",JSON.stringify(friends));
        items = JSON.parse(localStorage.getItem("friends"));
        alert("Congrats!!! New Friend Added");
        console.log("New Friend added.\nTotal Friends" + items.length +" : ");
        for(let i = 0;i<items.length;i++)
        {
            console.log(items[i].name + "     " +items[i].dob);
        }
        birthdayNextMonth();
        birthdayOnSunday();
        sameDayBirthday();
        upcomingBirthday();
        oldestFriend();
    }



    function userAccess() 
    {
        var init = prompt("Want to initialize predefined data (Yes/No)");
        if(init == 'Yes' || init == 'yes')
        {
            localstorage();
        }
        var option = prompt("Want to enter another friends details (Yes/No)");
        if(option == 'Yes' || option == 'yes')
        {
            addFriend();
        }
        else{
            alert("We suppose you entered No...See your results in console");
            birthdayNextMonth();
            birthdayOnSunday();
            sameDayBirthday();
            upcomingBirthday();
            oldestFriend();
        }
    }


    return userAccess;

}

var user = dataEnter();
document.getElementById("start").onclick = ()=>{
    user();
}