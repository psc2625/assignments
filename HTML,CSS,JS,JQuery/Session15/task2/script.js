function main()
{
    var imageDivConfigObj = imageDivConfig().imageDiv;
    var randomIndex = 0;
    var imageDivConfigLen = imageDivConfigObj.length;

    $('.imageDiv').on('click' , 'div', (index)=>{
        let className = index.target.className;
        if(className == imageDivConfigObj[randomIndex].className)
        {
            confirmToPlay();
        }
        else
        {
            continueToTry();
        }
    } )

    function displayDiv(){
        for(let i = 0; i < imageDivConfigLen; i++)
        {
            let divClass = imageDivConfigObj[i].className;
            $('.imageDiv').append(`<div class="${divClass}"></div>`);
        }
        game();
    }

    function game() {
        if(imageDivConfigObj.length == 0)
        {
            $('body').html("No more questions to ask </br> Thanks For Playing!!!");
        }
        var imageObj = generateRandomQuestion();
        alert('Click on '+imageObj.className);
    }

    function generateRandomQuestion() {
        imageDivConfigLen = imageDivConfigObj.length;
        randomIndex = Math.floor(Math.random() * imageDivConfigLen);
        return imageDivConfigObj[randomIndex];
    }

    function confirmToPlay() {
        alert('Correct Choice. Congrats!!!');
        let confirms = confirm('Want to Play!!!');
        if (confirms) {
            let removeItem = imageDivConfigObj[randomIndex];
            imageDivConfigObj = $.grep(imageDivConfigObj , (value) => {
                return value !=  removeItem;
            });
            game();
        }
        else{
            $('body').html("Thanks For Playing!!!");
        }
    }

    function continueToTry() {
        alert("Try Again");
    }

    return {
       displayDiv: displayDiv
    }
}

$(document).ready(()=>{
    var start = main();
    start.displayDiv();
})
