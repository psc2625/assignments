function imageDivConfig() {
    var imageDiv = [
        {
            className: 'circle'
        },
        {
            className: 'rectangle'
        },
        {
            className: 'triangle'
        },
        {
            className: 'square'
        },
        {
            className: 'cloud'
        },
        {
            className: 'flowerPot'
        },
        {
            className: 'tree'
        },
        {
            className: 'crow'
        }
    ];

    return {
        imageDiv: imageDiv
    }
}