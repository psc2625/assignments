function replace() {
    let str = document.getElementById("inputString").value;
    let updatedString = "";
    let replacedCharacter = document.getElementById("replacedCharacter").value;
    let replacableCharacter = document.getElementById("replacableCharacter").value;
    if (replacedCharacter.length == 1 && replacedCharacter.length == 1) {
        for (let i = 0; i < str.length; i++) {
            if (str[i] == replacedCharacter) {
                updatedString = updatedString + replacableCharacter;
            }
            else {
                updatedString = updatedString + str[i];
            }
        }
        document.getElementById("output").value = updatedString;
    }
    else{
        alert("Enter only one character in 'Enter the letter to be replaced' and 'Enter the letter to be replaced by' box");
    }
}