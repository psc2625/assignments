const modelTable = {
	Maruti: [
		'800 (1995)',
		'Alto (2000)',
		'WagonR (2002)',
		'Esteem (2004)',
		'SX4 (2007)',
	],
	TATA: ['Indica (2001)', 'Indigo (2006)', 'Safari (2003)', 'Sumo (2001)'],
	Chevrolet: ['Beat (2006)', 'Travera (2002)', 'Spark (2007)'],
	Toyota: [
		'Camry (2005)',
		'Etios (2010)',
		'Corolla (2003)',
		'Endeavour (2008)',
	],
};
let list = [];
$(document).ready(function () {
	$('#toggle_button').click(function () {
		const role = $(this).data('role');
		if (role === 0) {
			$('.form_container').hide();
			$('.table_container').show();
			$(this).data('role', 1);
			$('#toggle_button').text('Show Form');
			populateTable();
		} else {
			$('.form_container').show();
			$('.table_container').hide();
			$(this).data('role', 0);
			$('#toggle_button').text('Show Table');
		}
	});
	$('#make').change(function () {
		const selectedMake = $(this).children('option:selected').val();
		const model = $('#model');
		model.html('<option selected disabled value="">select model</option>');
		modelTable[selectedMake].forEach((element) => {
			model.append(`<option value="${element}">${element}</option>`);
		});
	});
	$('#model').change(function () {
		const selectedModel = $(this).children('option:selected').val();
		const year = $('#year');
		year.html('<option selected disabled value="">select year</option>');
		const yearTable = [];
		const extYear = selectedModel.split(' ')[1].replace(/\(|\)/g, '');
		const date = new Date();
		for (let i = extYear; i <= date.getFullYear(); i++) {
			yearTable.push(i);
		}
		yearTable.forEach((element) => {
			year.append(`<option value="${element}">${element}</option>`);
		});
	});
	$('#form').submit(function (e) {
		e.preventDefault();
		const firstName = $('#firstName').val();
		const lastName = $('#lastName').val();
		const email = $('#email').val();
		const contact = $('#contact').val();
		const make = $('#make').val();
		const model = $('#model').val();
		const year = $('#year').val();
		const color = $('#color').val();
		const problems = $('#problems').val();
		const id = Date.now().toString();
		const listData = {
			id,
			firstName,
			lastName,
			email,
			contact,
			make,
			model,
			year,
			color,
			problems,
		};
		list.push(listData);
		$('.message').html(
			"You're car has been added to our garage for servicing.",
		);
		$('.message').addClass('show_message');
		setTimeout(() => {
			$('.message').removeClass('show_message');
		}, 5000);
	});
	function populateTable() {
		$('#table').html(`
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Contact Number</th>
            <th>Make</th>
            <th>Model</th>
            <th>Year</th>
            <th>Color</th>
            <th>Problems</th>
            <th>Mark Completed</th>
        </tr>
    `);
		list.reverse().forEach((element) => {
			$('#table').append(`
            <tr>
                <td>${element.firstName}</td>
                <td>${element.lastName}</td>
                <td>${element.email}</td>
                <td>${element.contact}</td>
                <td>${element.make}</td>
                <td>${element.model}</td>
                <td>${element.year}</td>
                <td>${element.color}</td>
                <td>${element.problems}</td>
                <td><input type='checkbox' class="checkbox_action" value="${element.id}"/></td>
            </tr>
            `);
			$('.checkbox_action').change(function () {
				const val = $(this).val();
				list = list.filter((item) => item.id !== val);
				populateTable();
			});
		});
	}
});
