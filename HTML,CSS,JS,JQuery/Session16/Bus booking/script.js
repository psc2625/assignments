function booking() {
    var arr = [];
    var selected = [];
    var booked = [];
    var seat = {
        seatNumber: 0,
        seatstatus: 'available'
    };
    var amount;
    var property = 'background-color';

    $('.booking-head').click(() => {
        $('.booking-head').css('background-color', '#5f9ea0')
        $('.payment-head').css('background-color', 'inherit')
        $('.confirm-head').css('background-color', 'inherit')
        $('.booking').css({
            display: 'block'
        })
        $('.payment').css({
            display: 'none'
        })
        $('.confirm').css({
            display: 'none'
        })
    })

    $('.payment-head').click(() => {
        if(selected.length === 0){
            alert('Select seats first');
            return;
        }
        amount = 0;
        $('.payment-head').css('background-color', '#5f9ea0')
        $('.booking-head').css('background-color', 'inherit')
        $('.confirm-head').css('background-color', 'inherit')
        $('.booking').css({
            display: 'none'
        })
        $('.payment').css({
            display: 'block'
        })
        $('.confirm').css({
            display: 'none'
        })
        for (i = 0; i < selected.length; i++) {
            if (selected[i].seatNumber <= 4 || (15 <= selected[i].seatNumber && selected[i].seatNumber <= 19)) {
                amount += 990;
            }
            else {
                amount += 900;
            }
        }
        $('.total-amount').text(amount);
    })

    $('.confirm-head').click(() => {
        if(selected.length === 0){
            alert('Select seats first');
            return;
        }
        $('.payment-head').css('background-color', 'inherit')
        $('.booking-head').css('background-color', 'inherit')
        $('.confirm-head').css('background-color', '#5f9ea0')
        $('.booking').css({
            display: 'none'
        })
        $('.payment').css({
            display: 'none'
        })
        $('.confirm').css({
            display: 'block'
        })
    })

    $('.bus').on('click', 'div', (index) => {
        var idName = index.target.id;
        if(arr[idName].seatstatus === 'booked')
        {
            alert('Seat already booked. Select another seat');
            return;
        }
        if (arr[idName].seatstatus === 'selected') {
            $('#' + idName).css("background-color", "green");
            selected = selected.filter(index =>
                index.seatNumber != idName
            );
            arr[idName].seatstatus = 'available';
            console.log(selected);
            return;
        }
        arr[idName].seatstatus = 'selected';
        $('#' + idName).css("background-color", "grey");
        selected.push(arr[idName]);
    })

    $('.pay-now').click(() => {
        if(selected.length === 0){
            alert('Select seats first');
            return;
        }
        amount = 0;
        $('.payment-head').css('background-color', '#5f9ea0')
        $('.booking-head').css('background-color', 'inherit')
        $('.confirm-head').css('background-color', 'inherit')
        $('.booking').css({
            display: 'none'
        })
        $('.payment').css({
            display: 'block'
        })
        $('.confirm').css({
            display: 'none'
        })
        for (i = 0; i < selected.length; i++) {
            if (selected[i].seatNumber <= 4 || (15 <= selected[i].seatNumber && selected[i].seatNumber <= 19)) {
                amount += 990;
            }
            else {
                amount += 900;
            }
        }
        $('.total-amount').text(amount);
    })

    $('.confirm-btn').click(()=>{
        alert("Thank you for booking");
        $('.booking-head').css('background-color', '#5f9ea0')
        $('.payment-head').css('background-color', 'inherit')
        $('.confirm-head').css('background-color', 'inherit')
        $('.booking').css({
            display: 'block'
        })
        $('.payment').css({
            display: 'none'
        })
        $('.confirm').css({
            display: 'none'
        })
        for(let i =0 ; i < arr.length ; i++){
            if(arr[i].seatstatus == 'selected')
            {
                let idName = arr[i].seatNumber;
                $('#' + idName).css("background-color", "red");
                arr[i].seatstatus = 'booked';   
            }
        }
        selected = [];
    })

    $('.book-another').click(()=>{
        $('.booking-head').css('background-color', '#5f9ea0')
        $('.payment-head').css('background-color', 'inherit')
        $('.confirm-head').css('background-color', 'inherit')
        $('.booking').css({
            display: 'block'
        })
        $('.payment').css({
            display: 'none'
        })
        $('.confirm').css({
            display: 'none'
        })
        for(let i =0 ; i < arr.length ; i++){
            if(arr[i].seatstatus == 'selected')
            {
                let idName = arr[i].seatNumber;
                $('#' + idName).css("background-color", "red");
                arr[i].seatstatus = 'booked';   
            }
        }
        selected = [];
    })


    function displaySeat() {
            for (let i = 0; i < 10; i++) {
                $('.bus').append(`<div class="seat" id = '${i}'></div>`);
                seat = {
                    seatNumber: i,
                    seatstatus: 'available'
                };
                arr.push(seat);
            }
            for (let i = 0; i < 5; i++) {
                $('.bus').append(`<br/>`);
            }
            for (let i = 10; i < 20; i++) {
                $('.bus').append(`<div class="seat" id = '${i}'></div>`);
                seat = {
                    seatNumber: i,
                    seatstatus: 'available'
                };
                arr.push(seat);
                
            }
    }

    $('#form').submit(function (e) {
		e.preventDefault();
        $('.payment-head').css('background-color', 'inherit');
        $('.booking-head').css('background-color', 'inherit');
        $('.confirm-head').css('background-color', '#5f9ea0');
        $('.booking').css({
            display: 'none'
        });
        $('.payment').css({
            display: 'none'
        });
        $('.confirm').css({
            display: 'block'
        });
        if (selected) {
            $("#no-of-seats").text(selected.length);
            for (let i = 0; i < selected.length; i++) {
                $("#seat-number").append(selected[i].seatNumber);
                if (i + 1 < selected.length) {
                    $("#seat-number").append(", ");
                }
            }
            $("#amount-paid").text(amount);
        }
    }

    function submits() {
    }


    return {
        displaySeat: displaySeat,
        submits: submits
    };

}
$(document).ready(() => {
   var book = booking();
   book.displaySeat();
})