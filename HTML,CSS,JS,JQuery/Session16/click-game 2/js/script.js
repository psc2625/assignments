function CircleGame() {
    var score = 0;
    var difficultyValue = 'easy';
    var shapeValue = 'circle';
    var timeLimit = 60;
    var appearingTime = 1000;
    var timerInterval;
    var height;
    var width;
    var circleGenerateInterval;
    var scoreConfigObj = ScoreConfig().scoreData;

    $('.play-area').on('click', 'div', (index) =>{
        let className = index.target.className;
        let scoreOfClass = getScoreByClass(className);
        score += scoreOfClass;
        $('.total-score').text(score);
    });

    $('#submit').click(() => {
        timeLimit = document.getElementById("duration").value;
        if(timeLimit > 180)
        {
            alert('Enter value less than 180 seconds');
            window.location.reload();
        }
        var difficulty = document.getElementsByName("difficulty");
        for(i = 0; i < difficulty.length; i++)
        {
            if(difficulty[i].checked)
            {
                difficultyValue = difficulty[i].value;
            }
        }
        if(difficultyValue === 'easy')
        {
            height = 460;
            width = 580;
            $('.play-area').css({
                display: 'block',
                height: '480px',
                width : '600px'
            })
            appearingTime = 1200;
        }
        else if(difficultyValue === 'medium')
        {
            height = 580;
            width = 780;
            $('.play-area').css({
                display: 'block',
                height: '600px',
                width : '800px'
            })
            appearingTime = 1000;
        }
        else if(difficultyValue === 'hard')
        {
            height = 748;
            width = 1004;
            $('.play-area').css({
                display: 'block',
                height: '768px',
                width : '1024px'
            })
            appearingTime = 750;
        }
        var shape = document.getElementsByName("shape");
        for(i = 0; i < shape.length; i++)
        {
            if(shape[i].checked)
            {
                shapeValue = shape[i].value;
            }
        }
        $('.inputDetails').css({
            display: 'none'
        })
        $('.play-area').css({
            display: 'block'
        })
        $('.scoreboard').css({
            display: 'block'
        })
        startGame();

    })

    function startGame() {
        timerInterval = setInterval(function() {
            timeLimit = timeLimit - 1;
            $('.timer').text(timeLimit);
            $('.total-score').text(score);
            if (timeLimit <= 0) {
                endGame();
            }
        }, 1000);
        circleGenerateInterval = setInterval(function() {
            var circlePosition = generateRandomPosition();
            var circleObj = generateRandomCircle();
            $('.play-area').html(`<div class="${circleObj.className}"></div>`);
            if(shapeValue === 'square')
            {
                $('.play-area div').css("border-radius","0px");
            }
            $('.' + circleObj.className).css({
                position: 'absolute',
                left: circlePosition.x + 'px',
                top: circlePosition.y + 'px'
            });
        }, appearingTime);
    }


    function endGame () {
        clearInterval(timerInterval);
        clearInterval(circleGenerateInterval);
        $('.play-area').html('');
        $('body').html('GAME OVER!!! <br /> Your total score : '+score);
    }

    function generateRandomPosition() {
        var x = Math.floor((Math.random() * width) + 1);
        var y = Math.floor((Math.random() * height) + 1);
        return {
            x: x,
            y: y
        }
    }

    function generateRandomCircle() {
        var scoreConfigLen = scoreConfigObj.length;
        var randomIndex = Math.floor((Math.random() * scoreConfigLen) + 0);
        return scoreConfigObj[randomIndex];
    }

    function getScoreByClass(className) {
        let scoreObj = scoreConfigObj.filter((index) => {
            return className === index.className ? true : false;
        })

        console.log(scoreObj);

        if(scoreObj === null)
        {
            return 0;
        }
        else
        {
            return scoreObj[0].score;
        }
    }

    return {
    }
}

$(document).ready(()=>{    
    CircleGame();
})