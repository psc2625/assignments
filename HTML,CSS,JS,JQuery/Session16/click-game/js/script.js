function CircleGame() {
    var score = 0;
    var timeLimit = 60;
    var timerInterval;
    var circleGenerateInterval;
    var scoreConfigObj = ScoreConfig().scoreData;

    $('.play-area').on('click', 'div', (index) =>{
        let className = index.target.className;
        let scoreOfClass = getScoreByClass(className);
        score += scoreOfClass;
        $('.total-score').text(score);
    });

    function startGame() {
        timerInterval = setInterval(function() {
            timeLimit = timeLimit - 1;
            $('.timer').text(timeLimit);
            $('.total-score').text(score);
            if (timeLimit === 0) {
                endGame();
            }
        }, 1000);
        circleGenerateInterval = setInterval(function() {
            var circlePosition = generateRandomPosition();
            var circleObj = generateRandomCircle();
            $('.play-area').html(`<div class="${circleObj.className}"></div>`);
            $('.' + circleObj.className).css({
                position: 'absolute',
                left: circlePosition.x + 'px',
                top: circlePosition.y + 'px'
            });
        }, 1000);
    }


    function endGame () {
        clearInterval(timerInterval);
        clearInterval(circleGenerateInterval);
        $('.play-area').html('');
        $('body').html('GAME OVER!!! <br /> Your total score : '+score);
    }

    function generateRandomPosition() {
        var x = Math.floor((Math.random() * 800) + 1);
        var y = Math.floor((Math.random() * 600) + 1);
        return {
            x: x,
            y: y
        }
    }

    function generateRandomCircle() {
        var scoreConfigLen = scoreConfigObj.length;
        var randomIndex = Math.floor((Math.random() * scoreConfigLen) + 0);
        return scoreConfigObj[randomIndex];
    }

    function getScoreByClass(className) {
        let scoreObj = scoreConfigObj.filter((index) => {
            return className === index.className ? true : false;
        })

        console.log(scoreObj);

        if(scoreObj === null)
        {
            return 0;
        }
        else
        {
            return scoreObj[0].score;
        }
    }

    return {
        startGame: startGame
    }
}

$(document).ready(()=>{    
    var circleGameInstance = CircleGame();
    circleGameInstance.startGame();
})