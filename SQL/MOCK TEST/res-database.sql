DROP DATABASE IF EXISTS `RES-database`;
CREATE DATABASE `RES-database`; 
USE `RES-database`;

SET NAMES utf8 ;
SET character_set_client = utf8mb4 ;

CREATE TABLE `registration` (
  `customer_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` varchar(15) ,
  `birth_date` date DEFAULT NULL,
  `category` varchar(15) DEFAULT 'Normal',
  `address` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `friend_id` tinyint(4) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO registration (customer_id,first_name, last_name, gender, address, phone)
	   VALUES ('1','Prateek', 'Singh', 'Male', 'Lucknow', '9452919568'); 


CREATE TABLE `book_details` (
  `book_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(100) NOT NULL,
  `book_author` varchar(50) NOT NULL,
  `book_publisher` varchar(50) NOT NULL,
  `book_genre` varchar(50) NOT NULL,
  `release_year` date DEFAULT NULL,
  `rating` int(10) NOT NULL,
  `created_on` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO book_details (book_id, book_title, book_author, book_publisher, book_genre, rating)
	   VALUES ('1','ABCD', '1', '1','Novel','5');

CREATE TABLE `author_details` (
  `author_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(50) NOT NULL,
  `birth_date` date DEFAULT NULL,
  `description` varchar(200),
  `created_on` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO  author_details (author_id, author_name, birth_date, description)
	   VALUES( '1', 'ABCD', '2020-01-01', 'description');

CREATE TABLE `rented`(
  `customer_id` tinyint(4) NOT NULL,
  `book_id` tinyint(4) NOT NULL,
  `rent_status` varchar(10) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `liked`(
  `customer_id` tinyint(4) NOT NULL,
  `book_id` tinyint(4) NOT NULL,
  `like_status` varchar(10) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `wishlishted`(
  `customer_id` tinyint(4) NOT NULL,
  `book_id` tinyint(4) NOT NULL,
  `wishlisht_status` varchar(10) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `publisher_details` (
  `publisher_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `publisher_name` varchar(50) NOT NULL,
  `established_date` date DEFAULT NULL,
  `address` varchar(50) NOT NULL,
  `description` varchar(50) ,
  `created_on` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `status` tinyint(4) default '1',
  PRIMARY KEY (`publisher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO publisher_details (
			publisher_id,
			publisher_name,
            address)
	   VALUES( '1','ABC',
			   'Hyderabad');