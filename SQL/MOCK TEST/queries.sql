USE `res-database`;

-- 1
INSERT INTO  author_details (
			 author_name,
			 birth_date,
             description)
	   VALUES( 'AGATHA CHRISTIE',
			   '1890-09-15',
               'An English writer known for her sixty-six detective novels and fourten short story collections particularly those resolving around fictional detectives Hercule Poirot and Miss Marple');


-- 2
INSERT INTO publisher_details (
			publisher_name,
            address)
	   VALUES( 'Penguin Random House',
			   'New York City, US'),
			 ('Hachette Livre',
              'Paris, France');


-- 3
UPDATE book_details
SET book_genre = 'Mystery, Fiction'
WHERE book_id = '1'; 


-- 4
INSERT INTO book_details ( book_title, book_author, book_publisher, book_genre, rating)
	   VALUES ('Murder on the Orient Express', '2', '2', 'Novel, Mystery, Detective Fiction', '4.6');
       
-- 5
INSERT INTO registration ( first_name, last_name, gender, birth_date, category, address, phone, friend_id)
			VALUES ('Ranjeet', 'Shukla', 'Male', '1998-04-13', 'Premium', 'Lucknow', '8698684585', '1');
            
            
-- 6 
INSERT INTO rented (customer_id, book_id, rent_status)
			VALUES('2', '2', 'rent');
            
            
-- 7
INSERT INTO liked (customer_id, book_id,like_status)
			VALUES('2', '2', 'like');
            

-- 8
UPDATE registration
SET friend_id = '2'
WHERE customer_id = '1';


-- 9
INSERT INTO wishlisted (customer_id, book_id,wishlist_status)
			VALUES('1', '2', 'wishlist');
            
            
-- 10
SELECT * FROM registration 
WHERE category != 'Premium';


-- 11
SELECT * FROM registration 
WHERE gender = 'Female';


-- 12
Select book_genre
FROM book_details;


-- 13
Select *
FROM book_details
WHERE rating > 4;


-- 14
Select *
FROM book_details
ORDER BY rating DESC;


-- 15
Select *
FROM book_details
ORDER BY rating;


-- 16
SELECT *
FROM author_details
WHERE author_name REGEXP '^AR';


-- 17
SELECT *
FROM publisher_details
WHERE established_date < 2012;


-- 18
Select *
FROM book_details
ORDER BY rating DESC
LIMIT 5;


-- 19
SELECT x.first_name
FROM registration x
JOIN registration f
	ON x.friend_id = f.customer_id
WHERE x.customer_id = '1';


-- 20
SELECT *
FROM book_details
WHERE release_year BETWEEN 2012 AND 2018;


-- 21
SELECT *
FROM registration
WHERE (category = 'Premium') AND (friend_id);


-- 22
Select a.book_genre, COUNT(a.book_genre)
FROM book_details a
GROUP BY a.book_genre
HAVING COUNT(a.book_genre) = (SELECT MAX(mycount) 
							FROM (	SELECT b.book_genre, COUNT(b.book_genre) mycount
                            		FROM book_details b
                                    GROUP BY b.book_genre) c);
 

-- 23
SELECT b.book_title, a.author_name
FROM book_details b
JOIN author_details a
	ON b.book_author = a.author_id;
    
    
-- 24
SELECT book_id,book_title,book_author,book_publisher,book_genre,release_year
FROM book_details
JOIN liked l
	USING(book_id)
JOIN registration r
	ON r.friend_id = l.customer_id
WHERE l.like_status = 'liked'AND r.costumer_id = '1';


-- 25
SELECT book_id,book_title,book_author,book_publisher,book_genre,release_year
FROM book_details
JOIN rented rd
	USING(book_id)
JOIN registration r
	ON r.friend_id = rd.customer_id
WHERE rd.rent_status = 'rented' AND r.costumer_id = '1';


-- 26
SELECT book_id,book_title,book_author,book_publisher,book_genre,release_year
FROM book_details
JOIN wishlisted w
	USING(book_id)
JOIN registration r
	ON r.friend_id = w.customer_id
WHERE w.wishlist_status = 'wishlisted' AND r.costumer_id = '1';


-- 27
UPDATE registration
SET category = 'Premium'
WHERE customer_id = (SELECT rg.customer_id
					FROM registration rg
					JOIN rented rd
						USING(customer_id)
					HAVING count(rd.customer_id) = 10);


-- 28 
UPDATE registration
SET category = 'Premium'
WHERE count((SELECT *
			 FROM rented rd
             GROUP BY rd.customer_id) = 5);
				
                
-- 30
SELECT * 
FROM publishers
WHERE count((SELECT *
			 FROM publishers p
             JOIN books_details bd
				on bd.book_publisher = p.publisher_id
             GROUP BY bd.book_genre) = 5);


-- 31
DELETE rg
FROM registration rg
INNER JOIN rented rd
	ON  rd.customer_id = rg.friend_id
WHERE rd.rent_status = 'rent';

-- 32
UPDATE registration rg
JOIN rented rd
	ON  rd.customer_id = rg.friend_id
SET status = '0'
WHERE rd.rent_status = 'rent';
